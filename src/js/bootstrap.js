import * as React from "react";
import * as ReactDOM from 'react-dom';

import { MainComponent } from './components/main.jsx';

const domContainer = document.querySelector('#app');

ReactDOM.render(React.createElement(MainComponent), domContainer);