import * as React from 'react';
import { format } from "date-fns";

export class LoaderComponent extends React.Component {
    render() {
        return (
            <h1>Hello World1! Current time : {format(new Date())}</h1>
        );
    }
}