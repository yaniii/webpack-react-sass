const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: [
        './src/js/bootstrap.js',
        './src/sass/main.sass'
    ],
    output: {
        filename: './js/main.min.js',
    },
    mode: "production",
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react']
                    }
                },
                exclude: /node_modules/,
            },
            {
                test: /\.(sass|scss)$/,
                include: path.resolve(__dirname, 'src/sass'),
                use: ExtractTextPlugin.extract({
                    use: [{
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                            minimize: true,
                            url: false
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                            minimize: true
                        }
                    },
                    ]
                })
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: './css/main.min.css',
            allChunks: true,
        }),
        new CopyWebpackPlugin([
            {
                context: path.resolve(__dirname),
                from: './src/index.html',
                to: path.resolve(__dirname, './dist/index.html'),
            },
        ])
    ],
};


